var validator = require('validator');
var User = require('../models/user');
var bcrypt = require('bcryptjs');
var jwt = require('../services/jwt');
var fs = require('fs');
var path = require('path');
var cloudinary = require('cloudinary').v2

cloudinary.config({ 
	cloud_name: 'hh56q77lq', 
	api_key: '268857582848379', 
	api_secret: 'fI-g8I9vlqa7z4rH6wkkm3jzFzo' 
 });
 

var controller = {

	save: function(req, res){

		var data = req.body; 

		try{
			var validate_name = !validator.isEmpty(data.name);
			var validate_surname = !validator.isEmpty(data.surname);
			var validate_email = !validator.isEmpty(data.email) && validator.isEmail(data.email);
			var validate_password = !validator.isEmpty(data.password);
		}catch(error){
			return res.status(200).send({ status: 'success',  message : 'Invalid data', data });
		}

		if(validate_name && validate_surname && validate_email && validate_password){

			var user = new User();

			user.name = data.name;
			user.surname = data.surname;
			user.email = data.email.toLowerCase();
			user.name = data.name;
			user.role = 'ROLE_USER';
			user.image = '1562792799/avatar_mopsjz.jpg';

			User.findOne({email: user.email}, (error, issetUSer) => {
				
				if(error){
					return res.status(500).send({ status: 'error',  message : 'User exists' });
				}

				if(!issetUSer){
					bcrypt.genSalt(10, function(err, salt) {
						bcrypt.hash( data.password, salt, function(err, hash) {

							user.password = hash;

							user.save((error, userStored ) =>{
								
								if(error){
									return res.status(500).send({ status: 'error',  message : 'Error to save user' });
								}

								if(!userStored){
									return res.status(400).send({ status: 'error', message : 'User can´t save' });
								}

								return res.status(200).send({ status: 'success', message : 'User registered', user});
							});
						});
					});
				}else{
					return res.status(500).send({ status: 'error',  message : 'User already exists' });
				}
			});
		}else{
			return res.status(200).send({ status: 'error',  message : 'Invalid data' });
		}
	},

	login: function(req, res){

		var data = req.body; 

		try{
			var validate_email = !validator.isEmpty(data.email) && validator.isEmail(data.email);
			var validate_password = !validator.isEmpty(data.password);
		}catch(error){
			return res.status(200).send({ status: 'success',  message : 'Invalid data', data });
		}

		if(validate_email && validate_password){
			User.findOne({email: data.email.toLowerCase()}, (error, user) => {

				if(error){
					return res.status(500).send({ status: 'error', message : 'User login error' });
				}

				if(user){
					bcrypt.compare(data.password, user.password, function(error, check) {
						if(check){

							if(data.gettoken){
								return res.status(200).send({ status: 'success',  token : jwt.createToken(user) });

							}else{
								user.password = undefined;
								return res.status(200).send({ status: 'success',  message : 'Acces correct', user});
							}
						}else{
							return res.status(200).send({ status: 'error',  message : 'Email or password incorrect' });
						}
					});
				}else{
					return res.status(404).send({ status: 'error',  message : 'User not exists' });
				}

			});
		}else{
			return res.status(200).send({ status: 'error',  message : 'Invalid data' });
		}
	},

	update: function(req, res){

		var data = req.body;

		try{
			var validate_name = !validator.isEmpty(data.name);
			var validate_surname = !validator.isEmpty(data.surname);
		}catch(error){
			return res.status(200).send({ status: 'error',  message : 'Invalid data', data });
		}

		delete data.password;

		var userId = req.user.sub;

		if(req.user.email != data.email){
			
			User.findOne({email: data.email.toLowerCase()}, (error, user) => {

				if(error){
					return res.status(500).send({status: 'error', message: 'Unable to find user'})
				}

				if(user && user.email == data.email){
					return res.status(200).send({status: 'error', message: 'Unable to modify email'})

				}else{

					User.findOneAndUpdate({ _id:userId }, data, { new:true }, (error, userUpdate) =>{

						if(error){
							return res.status(500).send({ status: 'error',  message : 'Error', user: userUpdate });
						}
		
						if(!userUpdate){
							return res.status(500).send({ status: 'error',  message : 'Error user udpate', user: userUpdate });
						}
		
						return res.status(200).send({ status: 'succcess',  message : 'User update', user: userUpdate });
		
					})
					
				}
			})
		}
		
		User.findOneAndUpdate({ _id:userId }, data, { new:true }, (error, userUpdate) =>{

			if(error){
				return res.status(500).send({ status: 'error',  message : 'Error', user: userUpdate });
			}

			if(!userUpdate){
				return res.status(500).send({ status: 'error',  message : 'Error user udpate', user: userUpdate });
			}

			return res.status(200).send({ status: 'succcess',  message : 'User update', user: userUpdate });

		})
	},

	uploadAvatar: function(req, res){

		var fileName = 'Upload image....';
		
		if(!req.files){
			return res.status(404).send({ message : fileName });
		}

		var filePath = req.files.file0.path;

		var fileSplit = filePath.split('/');
		var fileName = fileSplit[2];

		var fileExtension = fileName.split('.');
		var fileExtension = fileExtension[1];

		if(req.files.file0.size > 1500000){

			return res.status(200).send({ status: 'error', message : 'Max size 1,5 Mb'	});

		}else if(fileExtension != 'png' && fileExtension != 'jpg' && fileExtension != 'jpeg' && fileExtension != 'gif'){

			fs.unlink(filePath, (error) =>{
				return res.status(200).send({ status: 'error', message : 'Invalid image type'	});
			});

		}else{

			var userId = req.user.sub;

			// cloudinary.uploader.upload( filePath, function(error, result) { console.log(result) });

			cloudinary.uploader.upload( filePath, (error, uploadImage) => {

				if(error){
					return res.status(500).send({ status : 'error', message : 'Error uploading image' });
				}

				var fileNameDatabase = uploadImage.version+'/'+uploadImage.public_id+'.'+uploadImage.format;

				User.findOneAndUpdate({ _id:userId }, { image: fileNameDatabase }, { new:true }, (error, userUpdate) => {
					if(error || !userUpdate){
						return res.status(500).send({ status : 'error', message : 'Error update user avatar' });
					}
					return res.status(200).send({ status : 'success', user: userUpdate });
				})

			});





		}
	},

	avatar: function(req, res){
		
		var fileName = req.params.fileName;
		var pathFile = './uploads/users/'+fileName;
		
		fs.exists(pathFile, (exists) => {
			if(exists){
				return res.sendFile(path.resolve(pathFile));
			}else{
				return res.status(404).send({ status : 'error', message: 'File not found' });
			}
		});

	},

	getUsers: function(req, res){

		User.find().exec( (error, users) => {
			if( error || !users){
				return res.status(404).send({ status: 'error', message : 'Users not found' });
			}

			return res.status(200).send({ status : 'succes', users });
		});
	},

	getUser: function(req, res){

		var userId = req.params.userId;

		User.findById(userId).exec( (error, user) => {
			
			if( error || !user ){
				return res.status(400).send({ status: 'error', message : 'User not found' });
			}

			return res.status(200).send({ status : 'succes', user });
		});
	}

};

module.exports = controller;