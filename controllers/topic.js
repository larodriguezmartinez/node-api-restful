var Topic = require('../models/topic');
var validator = require('validator');


var controller = {

   save: function(req, res){

      var data = req.body;

      try{

         var validateTitle = !validator.isEmpty(data.title);
         var validateContent = !validator.isEmpty(data.content);
         var validateLang = !validator.isEmpty(data.lang);

      }catch(error){
         return res.status(200).send({ message: 'Incomplete data' });
      }

      if(validateContent && validateContent && validateLang){

         var topic = new Topic();
         topic.title = data.title;
         topic.content = data.content;
         topic.code = data.code;
         topic.lang = data.lang.toLowerCase();
         topic.user = req.user.sub;

         topic.save( (error, topicStored) => {

            if( error || !topicStored ){
               return res.status(404).send({ status : 'error',  message : 'Topic not created' });
            }

            return res.status(200).send({ status : 'success', topic : topicStored });

         });

      }else{
         return res.status(200).send({ status : 'error', message: 'Invalid data' });
      }
   },

   getTopics: function(req, res){

      if( !req.params.page  || req.params.page == null || req.params.page == 0 || req.params.page == "0" || req.params.page == undefined ){
         var page = 1;
      }else{
         var page = parseInt(req.params.page);
      }

      var paginateOptions = {
         sort : { date: -1 },
         populate: 'user',
         limit: 7,
         page: page,
         collation: { locale: 'es' }
      };

      Topic.paginate({}, paginateOptions, (error, topics) => {

         if(error){
            return res.status(500).send({ status: 'error', message: 'Unable to find topics' });
         }

         if(!topics){
            return res.status(404).send({ status: 'error', message: 'No topics found' });
         }

         return res.status(200).send({ 
            status: 'success',
            message: 'Topics lists',
            topics: topics.docs,
            totalDocs: topics.totalDocs,
            totalPages: topics.totalPages
         })

      });

   },

   getTopic: function(req, res){

      var topidId = req.params.id;

      Topic.findById(topidId).populate('user').populate('comments.user').exec( (error, topic) => {

         if(error){
            return res.status(500).send({ status: 'error', message: 'Unable to find topic' });
         }
         
         if(!topic){
            return res.status(404).send({ status: 'error', message: 'Unable to find topic' });
         }

         return res.status(200).send({ 
            message: 'Get Topic', 
            status: 'succes',
            topic
         })

      });
   },

   getTopicsByUser: function(req, res){

      var userId = req.params.user;

      Topic.find({ user: userId }).sort([['date', 'descending']]).exec( (error, topics) => {

         if(error){
            return res.status(500).send({ status: 'error', message: 'Unable to find topics' });
         }

         if(!topics){
            return res.status(404).send({ status: 'error', message: 'No topics found' });
         }

         return res.status(200).send({ status: 'succes',  message: 'Get topics by user', topics });
      });
   },

   update: function(req, res){

      var topicId = req.params.id;

      var data = req.body;

      try{
         var validateTitle = !validator.isEmpty(data.title);
         var validateContent = !validator.isEmpty(data.content);
         var validateLang = !validator.isEmpty(data.lang);
      }catch(error){
         return res.status(200).send({ message: 'Incomplete data' });
      }

      if( validateTitle && validateContent && validateLang ){

         var update = {
            title: data.title,
            content: data.content,
            code: data.code,
            lang: data.lang
         }
   
         Topic.findOneAndUpdate({ _id:topicId, user: req.user.sub }, update, {new:true}, (error, topicUpdate) => {

            if(error){
               return  res.status(500).send({ status: 'error', message: 'Error on update'})
            }

            if(!topicUpdate){
               return  res.status(404).send({ status: 'error', message: 'Unable to update topic'})
            }
            
            return  res.status(200).send({ status: 'success', message: 'Topic update'})
         });

      }else{
         return  res.status(200).send({ status: 'success', message: 'Invalid data'})
      }

      
   },

   delete: function(req, res){

      var topicId = req.params.id;

      Topic.findByIdAndDelete({ _id:topicId, user: req.user.sub }, (error, topicRemove) => {

         if(error){
            return  res.status(500).send({ status: 'error', message: 'Error on update'})
         }

         if(!topicRemove){
            return  res.status(404).send({ status: 'error', message: 'Unable to remove topic'})
         }
         
         return res.status(200).send({ status: 'success', message:'Topic delete', topic: topicRemove })
      })

   },

   search: function(req, res){

      var string = req.params.search;

      Topic.find({ "$or": [ 
         {"title": {"$regex": string, "$options": "i"} },
         {"content": {"$regex": string, "$options": "i"} },
         {"code": {"$regex": string, "$options": "i"} },
         {"lang": {"$regex": string, "$options": "i"} },
      ] }).populate('user').sort([['date', 'descending']]).exec((error, topics) => {

         if(error){
            return res.status(500).send({status: 'error', message:"Error"});
         }

         if(!topics){
            return res.status(404).send({status: 'error', message:"Unable to find topic"});
         }

         return res.status(200).send({ status:'success', message:'Topics find', topics })
      });

   }
}

module.exports = controller;