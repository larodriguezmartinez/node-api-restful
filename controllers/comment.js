var Topic = require('../models/topic');
var validator = require('validator');

var controller = {

   add: function(req, res){

      var topicId = req.params.topicId;

      Topic.findById(topicId).exec((error, topic) => {

         if(error){
            return res.status(500).send({ status: 'error', message: 'Error'});
         }

         if(!topic){
            return res.status(404).send({ status: 'error', message: 'Unable to find topic'});
         }

         if(req.body.content){

            try{
               var validateContent = !validator.isEmpty(req.body.content);
            }catch(error){
               return res.status(200).send({ status: 'error', message: 'Invalid data' });
            }

            if(validateContent){

               topic.comments.push({
                  user: req.user.sub,
                  content: req.body.content,
               });

               topic.save((error) => {

                  if(error){
                     return res.status(500).send({ status: 'error', message: 'Unable to save comment'});
                  }
         

                  Topic.findById(topic._id).populate('user').populate('comments.user').exec( (error, topic) => {

                     if(error){
                        return res.status(500).send({ status: 'error', message: 'Unable to find topic' });
                     }
                     
                     if(!topic){
                        return res.status(404).send({ status: 'error', message: 'Unable to find topic' });
                     }
            
                     return res.status(200).send({ 
                        message: 'Get Topic', 
                        status: 'succes',
                        topic
                     })
            
                  });

               });

            }else{
               return res.status(200).send({ message: 'Incomplete data' });
            }
         }
      });

   },

   update: function(req, res){

      var commentId = req.params.commentId;

      try{
         var validateContent = !validator.isEmpty(req.body.content);
      }catch(error){
         return res.status(200).send({ status: 'error', message: 'Invalid data' });
      }

      if(validateContent){

         Topic.findOneAndUpdate({"comments._id": commentId}, { "$set": { "comments.$.content": req.body.content }},{new:true},(error, topicUpdated) => {

            if(error){
               return res.status(500).send({ status: 'error', message: 'Error'});
            }
   
            if(!topicUpdated){
               return res.status(404).send({ status: 'error', message: 'Unable to find topic'});
            }

            return res.status(200).send({ status: 'success', message: 'Update comment', topic: topicUpdated});
         });  
      }
   },

   delete: function(req, res){

      var topicId = req.params.topicId;
      var commentId = req.params.commentId;

      Topic.findById(topicId, (error, topic) => {

         if(error){
            return res.status(500).send({ status: 'error', message: 'Error'});
         }

         if(!topic){
            return res.status(404).send({ status: 'error', message: 'Unable to find topic'});
         }

         var comment = topic.comments.id(commentId);
         
         if(comment){
            comment.remove();

            topic.save((error) => {
               if(error){
                  return res.status(500).send({ status: 'error', message: 'Error'});
               }

               Topic.findById(topic._id).populate('user').populate('comments.user').exec( (error, topic) => {

                  if(error){
                     return res.status(500).send({ status: 'error', message: 'Unable to find topic' });
                  }
                  
                  if(!topic){
                     return res.status(404).send({ status: 'error', message: 'Unable to find topic' });
                  }
         
                  return res.status(200).send({ status: 'success', message: 'Delete comment', topic})
         
               });
            });
            
         }else{
            return res.status(200).send({ status: 'success', message: 'Unable to find comment'});
         }
      });
   }
};

module.exports = controller;