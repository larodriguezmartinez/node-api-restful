var jwt = require('jwt-simple');
var moment = require('moment');
var dotenv = require('dotenv');

dotenv.config();


exports.createToken = function(user){

   var payload = {
      sub: user._id,
      name: user.name,
      surname: user.surname,
      email: user.email,
      role: user.role,
      image: user.image,
      iat: moment().unix(), 
      exp: moment().add(1, 'days').unix
   }

   return jwt.encode(payload, process.env.SECRET_KEY);
};