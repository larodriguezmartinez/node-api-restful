var mongoose = require('mongoose');
var dotenv = require('dotenv');
var app = require('./app');
var port = process.env.PORT || 5000;

dotenv.config();

mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true })
         .then(()=>{ 
            console.log('Connected'); 
            app.listen(port, () => {
                  console.log('Running on  http://localhost:' + process.env.PORT );
            });
         })
         .catch( error => console.log(error) )

