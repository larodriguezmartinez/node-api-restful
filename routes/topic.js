var express = require('express');
var TopicController = require('../controllers/topic');
var router = express.Router();
var middelwareAuth = require('../middelwares/auth');

router.post('/topic', middelwareAuth.auth, TopicController.save);
router.get('/topics/:page?', TopicController.getTopics);
router.get('/topic/:id', TopicController.getTopic);
router.get('/user-topics/:user', TopicController.getTopicsByUser);
router.put('/topic/:id',  middelwareAuth.auth,  TopicController.update);
router.delete('/topic/:id', middelwareAuth.auth, TopicController.delete);
router.get('/search/:search', TopicController.search);

module.exports = router;