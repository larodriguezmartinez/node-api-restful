var express = require('express');
var UserController = require('../controllers/user');
var multipart = require('connect-multiparty');
var router = express.Router();

var middelwareAuth = require('../middelwares/auth');
var middelwareUpload = multipart({ uploadDir: './uploads/users' })



router.post('/register', UserController.save);
router.post('/login', UserController.login);
router.put('/update', middelwareAuth.auth, UserController.update);
router.post('/upload-avatar', [ middelwareUpload, middelwareAuth.auth ],  UserController.uploadAvatar);
router.get('/avatar/:fileName', UserController.avatar);
router.get('/users/', UserController.getUsers);
router.get('/user/:userId', UserController.getUser);

module.exports = router;