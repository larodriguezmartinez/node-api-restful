var express = require('express');
var CommentController = require('../controllers/comment');
var router = express.Router();
var middelwareAuth = require('../middelwares/auth');

router.post('/comment/topic/:topicId', middelwareAuth.auth, CommentController.add);
router.put('/comment/:commentId', middelwareAuth.auth, CommentController.update);
router.delete('/comment/:topicId/:commentId', middelwareAuth.auth, CommentController.delete);

module.exports = router;