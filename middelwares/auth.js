var jwt = require('jwt-simple');
var moment = require('moment');
var dotenv = require('dotenv');

dotenv.config();

exports.auth = function(req, res, next){

   if(!req.headers.authorization){
      return res.status(403). send({
         message : "Header not exist"
      });
   }

   var token = req.headers.authorization.replace(/['"]+/g,'');

   try{
      var payload = jwt.decode(token, process.env.SECRET_KEY);

      if(payload.exp <= moment().unix()){
         return res.status(404). send({
            message : "Token expired"
         });
      }

   }catch(ex){

      return res.status(404). send({
         message : "Invalid token"
      });

   }

   req.user = payload;

   next();
}